import {Directive} from '@angular/core';
import {FormGroup, Validator, NG_VALIDATORS} from '@angular/forms';

// NG_VALIDATORS is a collection of services, and we are adding one more item to that list (LocationValidatorDirective).
// With multi:true you are adding item to the list, without multi option you are overriding NG_VALIDATORS with LocationValidatorDirective
@Directive({
  selector: '[validateLocation]',
  providers: [{provide: NG_VALIDATORS, useExisting: LocationValidatorDirective, multi: true}]
})
export class LocationValidatorDirective implements Validator {

  validate(formGroup: FormGroup): { [key: string]: any } {
    const addressControl = formGroup.controls['address'];
    // same as above
    // let addressControl = control.controls.address;
    const cityControl = formGroup.controls['city'];
    const countryControl = formGroup.controls['country'];
    const onlineUrlControl = (<FormGroup>formGroup.root).controls['onlineUrl'];

    if ((addressControl && addressControl.value &&
      cityControl && cityControl.value &&
      countryControl && countryControl.value) ||
      (onlineUrlControl && onlineUrlControl.value)) {
      // returning a null tells the validation system that this validator is passing
      return null;
    } else {
      // returning [key: string]: any } => validateLocation is key of type string and false is value of type any
      return {validateLocation: false};
    }
  }

}
