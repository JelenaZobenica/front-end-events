import {ActivatedRouteSnapshot, CanActivate, Router} from '@angular/router';
import {Injectable} from '@angular/core';
import {EventService} from '../../../services/event.service';

@Injectable()
export class EventRouteActivatorService implements CanActivate {

  constructor(private eventService: EventService, private router: Router) {}

  /*
  * canDeactivate => prevent user from leaving a page, e.g. this is helpful if you want to warn user
  * if they try to navigate away from a page before saving their data
  * */

  // this method prevents user to navigate to a page, e.g. if event with id=1 exists user can navigate to a page,
  // if event with id=41 doesn't exist he can't navigate
  canActivate(route: ActivatedRouteSnapshot): boolean {
    // !! => cast object to a boolean
    const eventExist = !!this.eventService.getEvent(+route.params['id']);
    // console.log(eventExist + ' eventExist');

    if (!eventExist) {
      this.router.navigate(['/404']);
    }
    return eventExist;
  }
}
