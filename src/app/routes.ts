import {
  EventsListComponent,
  EventDetailsComponent,
  CreateEventComponent,
  CreateSessionComponent
} from 'app/components/events/index';

import {Routes} from '@angular/router';
import {Error404Component} from './components/errors/Error404.component';
import {EventResolverService} from './common/event-resolver.service';

export const appRoutes: Routes = [
  { path: 'events/new', component: CreateEventComponent, canDeactivate: ['canDeactivateCreateEvent'] },
  { path: 'events', component: EventsListComponent },
  // { path: 'events/:id', component: EventDetailsComponent, canActivate: [EventRouteActivatorService] },
  { path: 'events/:id', component: EventDetailsComponent, resolve: {event: EventResolverService} },
  { path: 'events/session/new', component: CreateSessionComponent },
  { path: '404', component: Error404Component },
  { path: 'user', loadChildren: 'app/components/user/user.module#UserModule' },
  { path: '', redirectTo: '/events', pathMatch: 'full' }
];
