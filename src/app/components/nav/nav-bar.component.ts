import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {ISession} from '../../models/session.model';
import {EventService} from '../../services';
import {IEvent} from '../../models/event.model';

@Component({
  selector: 'nav-bar',
  templateUrl: './nav-bar.component.html',
  styles: [`
    .nav.navbar-nav { font-size: 15px;}
    #searchForm { margin-right: 100px;}
    @media (max-width: 1200px) { #searchForm { display: none; }}
    li > a.active { color: #F97924; }
  `]
})
export class NavBarComponent implements OnInit {
  searchTerm: string = '';
  foundSessions: ISession[];
  events: IEvent[];

  constructor(public auth: AuthService, private eventService: EventService) {
  }

  ngOnInit() {
    this.eventService.getEvents().subscribe(events => {
      this.events = events;
    });
  }

  searchSessions(searchTerm: string) {
    this.eventService.searchSessions(searchTerm).subscribe(sessions => {
      this.foundSessions = sessions;
      this.searchTerm = '';
    });
  }
}
