import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {Injectable} from '@angular/core';
import {EventService} from '../services';

@Injectable()
export class EventResolverService implements Resolve<any> {

  constructor(private eventService: EventService) {}

  resolve(route: ActivatedRouteSnapshot): any {
    return this.eventService.getEvent(route.params['id']);
  }

}
