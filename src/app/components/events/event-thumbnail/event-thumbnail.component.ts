import {Component, Input} from '@angular/core';
import {IEvent} from '../../../models/event.model';

@Component({
  selector: 'event-thumbnail',
  templateUrl: './event-thumbnail.component.html',
  styles: [`
    .green { color: #003300 !important; }
    .bold { font-weight: bold; }
    .thumbnail { min-height: 210px; }
    .pad-left { margin-left: 10px; }
    .well div { color: #bbb }
  `]
})
export class EventThumbnailComponent {

  @Input()
  event: IEvent;

  getStartTimeClass() {
    // One solution
    /*const isEarlyStart = this.event && this.event.time === '8:00 am';
    return {green: isEarlyStart, bold: isEarlyStart};*/

    // Second solution
    if (this.event && this.event.time === '8:00 am') {
      // returning strings of CSS classes
     // return 'green bold';
      // returning array of CSS classes
      return ['green', 'bold'];
    }
    // returning empty string
    // return '';
    // returning empty array
      return [];
  }

  getStartTimeStyle() {
    if (this.event && this.event.time === '8:00 am') {
      return {color: '#003300', 'font-weight': 'bold'};
    }
    return {};
  }
}
