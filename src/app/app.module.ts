import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {
  EventsListComponent,
  EventThumbnailComponent,
  CreateEventComponent,
  EventDetailsComponent,
  EventRouteActivatorService,
  EventService,
  TOASTR_TOKEN,
  Toastr,
  CreateSessionComponent,
  SessionListComponent,
  UpvoteComponent,
  JQ_TOKEN,
  AuthService,
  VoterService
} from 'app/index';

import { EventsAppComponent } from './events-app.component';
import {NavBarComponent} from './components/nav/nav-bar.component';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {appRoutes} from './routes';
import {Error404Component} from './components/errors/Error404.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CollapsibleWellComponent} from './common/collapsible-well.component';
import {DurationPipe} from './common/duration.pipe';
import {SimpleModalComponent} from './common/simple-modal.component';
import {ModalTriggerDirective} from './common/modal-trigger.directive';
import {LocationValidatorDirective} from './validators/location-validator.directive';
import {HttpClientModule} from '@angular/common/http';
import {EventResolverService} from './common/event-resolver.service';

const toastr: Toastr = window['toastr'];
const jQuery = window['$'];

@NgModule({
  declarations: [
    EventsAppComponent,
    EventsListComponent,
    EventThumbnailComponent,
    NavBarComponent,
    EventDetailsComponent,
    CreateEventComponent,
    Error404Component,
    CreateSessionComponent,
    SessionListComponent,
    CollapsibleWellComponent,
    DurationPipe,
    SimpleModalComponent,
    ModalTriggerDirective,
    UpvoteComponent,
    LocationValidatorDirective
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    EventService,
    { provide: TOASTR_TOKEN, useValue: toastr },
    { provide: JQ_TOKEN, useValue: jQuery },
    EventRouteActivatorService,
    AuthService,
    VoterService,
    EventResolverService,
    {
      provide: 'canDeactivateCreateEvent',
      useValue: checkDirtyState
    }
  ],
  bootstrap: [EventsAppComponent]
})
export class AppModule { }

export function checkDirtyState(component: CreateEventComponent) {
  if (component.isDirty) {
    return window.confirm('You have not saved this event, do you really want to cancel?');
  }
  return true;
}
