import {Component, Inject, OnInit} from '@angular/core';
import {EventService} from '../../../services/event.service';
import {IEvent} from '../../../models/event.model';
import {Toastr, TOASTR_TOKEN} from '../../../services';

@Component({
  templateUrl: './events-list.component.html'
})
export class EventsListComponent implements OnInit {
  events: IEvent[];

  constructor(private eventsService: EventService, @Inject(TOASTR_TOKEN)private toastr: Toastr) {}

  ngOnInit() {
    // this.events = this.eventsService.getEvents();
    this.eventsService.getEvents().subscribe(events => {
      this.events = events;
    });
  }


  handleThumbnailClick(eventName: string) {
    this.toastr.success(eventName);
  }
}
