import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {EventService} from '../../../services/event.service';
import {IEvent} from '../../../models/event.model';
import {ISession} from '../../../models/session.model';

export interface IEvent {
  id: number;
  name: string;
  date: string;
  time: string;
  price: number;
  imageUrl: string;
  location: {
    address: string;
    city: string;
    country: string
  };
  sessions: any[];
}

@Component({
  templateUrl: './event-details.component.html',
  styles: [`
    .container { padding-left: 20px; padding-right: 20px;}
    .event-image { height: 100px; }
    a { cursor: pointer; }
  `]
})
export class EventDetailsComponent implements OnInit {
  event: IEvent;
  addMode: boolean;
  filterBy: string = 'all';
  sortBy: string = 'votes';

  constructor(private eventService: EventService, private route: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    // Whenever you are subscribing to the route parameters and using that as navigation to a different pages
    // within the same component you need keep a track of all pieces of state that exist in the page, e.g. addMode, filterBy, sortBy states.
    // GET EVENT TWO SOLUTIONS:
    // 1. Solution with subscription to the service
/*    this.route.params.forEach((params: Params) => {
      this.eventService.getEvent(+params['id']).subscribe((event: IEvent) => {
        this.event = event;
        this.addMode = false;
        this.filterBy = 'all';
        this.sortBy = 'votes';
      });
    });*/

    // 2. Solution with resolver
    this.route.data.forEach(data => {
      this.event = data['event'];
      this.addMode = false;
      this.filterBy = 'all';
      this.sortBy = 'votes';
    });

    // add + sign to converte string to a number
    /*const id = +this.route.snapshot.params['id'];
    this.event = this.eventService.getEvent(id);*/
  }

  addSession() {
    this.addMode = true;
  }

  saveNewSession(session: ISession) {
    let nextId = Math.max.apply(null, this.event.sessions.map(s => s.id));
    session.id = ++nextId;
    this.event.sessions.push(session);
    this.eventService.saveEvent(this.event).subscribe(() => {
      this.addMode = false;
    });
  }

  cancelAddSession() {
    this.addMode = false;
  }
}
