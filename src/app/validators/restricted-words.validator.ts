// custom validator for restricted words in abstract textarea field on Add Session page
import {FormControl} from '@angular/forms';

export function  restrictedWords(words) {
  return (control: FormControl): {[key: string]: any} => {
    if(!words) return null;

    const invalidWords = words.map(w => control.value.includes(w) ? w : null)
      .filter(w => w != null);
    return invalidWords && invalidWords.length > 0 ? {'restrictedWords': invalidWords.join(', ')} : null;
  };
}
